import numpy as np

def inv(a, mod):
    if mod == None:
        return 1 / a
    elif a == int(a):
        try:
            return pow(int(a), -1, mod)
        except:
            raise ValueError("Possible division by zero")
    else:
        raise TypeError("Tried to take find inverse of a float mod " + str(mod))

class Polynomial:
    def __init__(self, coefficients = None):
        if coefficients is None:
            self.coefficients = np.zeros(1)
        elif type(coefficients) is int or type(coefficients) is float:
            self.coefficients = np.array([coefficients])
        else:
            self.coefficients = np.array(coefficients)

    ### Boolean ###
    def __eq__(self, other):
        if type(other) is Polynomial:
            self._trim()
            other._trim()
            return np.array_equal(self.coefficients, other.coefficients)
        else:
            raise TypeError("Bad type in __eq__")

    ### Addition ###​
    def __add__(self, other, mod = None):
        if type(other) is Polynomial:
            return self._polynomial_add(other, mod)
        else:
            raise TypeError("Bad type in __add__")

    def __sub__(self, other, mod = None):
        if type(other) is Polynomial:
            return self.__add__(Polynomial(-1 * other.coefficients), mod)
        else:
            raise TypeError("Bad type in __sub__")

    def _polynomial_add(self, other, mod = None):
        if len(self.coefficients) < len(other.coefficients):
            padded_coeffs = np.append(self.coefficients,
                                      np.zeros(abs(len(self.coefficients) - len(other.coefficients))))
            coeffs = padded_coeffs + other.coefficients
        else:
            padded_coeffs = np.append(other.coefficients,
                                      np.zeros(abs(len(self.coefficients) - len(other.coefficients))))
            coeffs = padded_coeffs + self.coefficients

        if mod != None:
            coeffs %= mod
        p = Polynomial(coeffs)
        p._trim()
        return p

    ### Multiplication ###

    def __mul__(self, other, mod = None):
        if type(other) is Polynomial:
            return self._polynomial_mul(other, mod)
        else:
            raise TypeError("Bad type in __mul__")

    def _polynomial_mul(self, other, mod):
        acc = Polynomial([0])
        for i in range(len(self.coefficients)):
            if (self.coefficients[i] == 0):
                continue
            p = Polynomial(np.append(np.zeros(i), self.coefficients[i] * other.coefficients))
            acc = acc.__add__(p, mod)

        acc._trim()
        return acc

    ### Division ###

    def __floordiv__(self, other, mod = None):
        if type(other) is Polynomial:
            return self._divide_with_remainder(other, mod)[0]
        else:
            raise TypeError("Bad type in __truediv__")

    def __mod__(self, other, mod = None):
        if type(other) is Polynomial:
            return self._divide_with_remainder(other, mod)[1]
        else:
            raise TypeError("Bad type in __mod__")

    def _divide_with_remainder(self,other, mod = None):
        quotient = Polynomial(0)
        remainder = Polynomial(self.coefficients)
        while remainder.degree() >= other.degree() and not remainder == Polynomial(0):
            deg = remainder.degree() - other.degree()
            p_coeffs = np.zeros(deg + 1)
            p_coeffs[deg] = remainder.coefficients[remainder.degree()] * inv(other.coefficients[other.degree()], mod)
            p = Polynomial(p_coeffs)
            quotient = quotient.__add__(p, mod)
            remainder = remainder.__sub__(p.__mul__(other, mod), mod)
        return (quotient, remainder)

    ### Inverse ###

    # See https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm
    def inverse(self, other, mod = None):
        t, new_t = Polynomial(0), Polynomial(1)
        r, new_r = Polynomial(other.coefficients), Polynomial(self.coefficients)

        while new_r != Polynomial(0):
            quotient = r.__floordiv__(new_r, mod)
            (t, new_t) = new_t, t.__sub__(quotient.__mul__(new_t, mod), mod)
            (r, new_r) = new_r, r.__sub__(quotient.__mul__(new_r, mod), mod)

        if r.degree() > 0:
            raise ValueError("Inverse does not exist")

        return t.__mul__(Polynomial(1).__floordiv__(r, mod), mod)

    ### Misc. ###

    def degree(self):
        nonzero = np.flatnonzero(self.coefficients)
        if (len(nonzero) != 0):
            return int(nonzero[len(nonzero) - 1])
        else:
            return 0

    def _trim(self):
        nonzero = np.flatnonzero(self.coefficients)
        if (len(nonzero) != 0):
            last = int(nonzero[len(nonzero) - 1])
            self.coefficients = self.coefficients[0: last + 1]
        else:
            self.coefficients = np.zeros(1)

    def __str__(self):
        coeff_vars = [str(self.coefficients[i]) + "x^" + str(i) for i in range(len(self.coefficients))]
        coeff_vars.reverse()
        return " + ".join(coeff_vars)
def main():
    N = 251
    p = 3
    q = 257
    d = 5
    Fe=[]
    for i in range(0,252):
        if(i==251):
            Fe.append(1)
        elif(i==0):
            Fe.append(-1)
        else:
            Fe.append(0)
    F = Polynomial(Fe)
    print("F =",F)
    print("--------------")
    fe=[]
    for i in range(0,252):
        if(i==2 or i==8 or i==44 or i==46 or i==134):
            fe.append(-1)
        elif(i==57 or i==107 or i==188 or i==211 or i==249 or i==251):
            fe.append(1)
        else:
            fe.append(0)
    f = Polynomial(fe)
    print("f =",f)
    print("--------------")
    ge=[]
    for i in range(0,230):
        if(i==192 or i==128 or i==92 or i==74 or i==29):
            ge.append(-1)
        elif(i==229 or i==181 or i==103 or i==88 or i==33):
            ge.append(1)
        else:
            ge.append(0)
    g = Polynomial(ge)
    print("g =",g)
    print("--------------")
    me=[]
    for i in range(0,6):
        if(i==5 or i==1):
            me.append(-1)
        elif(i==0 or i==2 or i==3):
            me.append(1)
        else:
            me.append(0)
    m = Polynomial(me)
    print("m =",m)
    print("--------------")
    re=[]
    for i in range(0,5):
        if(i==0 or i==1 or i==2 or i==3):
            re.append(1)
        else:
            re.append(0)
    r = Polynomial(re)

    print("r =",r)
    print("--------------")
    inverse_f_p = f.inverse(F,p)
    inverse_f_q = f.inverse(F,q)
    print("The coefficients of f^{-1}_p(x) = ",inverse_f_p.coefficients)
    print("--------------")
    print("The coefficients of f^{-1}_q(x) = ",inverse_f_q.coefficients)
    print("--------------")
    print("The degree of f^{-1}_p(x) = ",inverse_f_p.degree())
    print("--------------")
    print("The degree of f^{-1}_q(x) = ",inverse_f_q.degree())
    print("--------------")
    hx = inverse_f_q.__mul__(g)
    print("The coefficients of h(x) =", hx.coefficients)
    print("The degree of h(x) = ", hx.degree())
    print("--------------")
    e_no_mod = ((Polynomial(p) * hx * r)+m)
    e = e_no_mod.__add__(e_no_mod,q)
    print("e = ",str(e))
if (__name__ == "__main__"):
    main()
